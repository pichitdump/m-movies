const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname, '/build')));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use("/api/m-movies/movies", require('./router/movies'));
app.use("/api/m-movies/history", require('./router/history'));
// app.get('/*', (req, res) => res.render('index'));
app.get('/*', function (req, res) {
    res.sendFile("index");
});

app.listen(8080, function () {
    console.log("Server is running...");
})