const express = require('express');
const router = express.Router();
const Email = require('../email/send');
const mongodb = require('../database/mongodb');
const db = new mongodb();
const option = {
    url: "mongodb://localhost:27017/",
    databaseName: "M-movies"
}
db.set = option;

router.route("/one")
    .get((req, res) => {
        db.find("movies")
            .then(r => {
                res.json(r);
            })
            .catch(err => {
                console.log(err)
                res.end(err.message);
            })
    })
    .post((req, res) => {
        const message = req.body;
        db.insertOne("movies", message)
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
    })
    .put((req, res) => {
        const { condition, values } = req.body;
        db.updateOne("movies", { condition, values })
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
            .catch(err => {
                console.log(err);
                res.end(err.message);
            })
    })
    .delete((req, res) => {
        const _id = db.decryObjectId(req.query._id);
        db.deleteOne("movies", { _id })
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
            .catch(err => {
                console.log(err);
                res.end(err.message);
            })
    });

router.route("/many")
    .get((req, res) => {
        const search = { name: { $regex: `.*${req.query.search}.*`, $options: 'i' } };
        db.find("movies", { condition: search })
            .then(r => {
                res.json(r);
            })
            .catch(err => {
                console.log(err)
                res.end(err.message);
            })
    })
    .post((req, res) => {
        const message = req.body;
        db.insertMany("movies", message)
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
    })
    .put((req, res) => {
        let { condition, values } = req.body;
        if (condition._id) {
            condition._id = db.decryObjectId(condition._id);
        }
        db.updateMany("movies", { condition, values })
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
            .catch(err => {
                console.log(err);
                res.end(err.message);
            })
    })
    .delete((req, res) => {
        let { condition } = req.body;
        db.deleteMany("movies", condition)
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
            .catch(err => {
                console.log(err);
                res.end(err.message);
            })
    });

router.route('/contact')
    .post((req, res) => {
        const { name, email, phone, message } = req.body;
        const _email = new Email();
        const option = {
            from: email,
            to: 'pichitkh58@gmail.com',
            subject: 'M-Movies Contact',
            text: `${message} จากคุณ ${name} ติดต่อ ${phone}`
        }
        _email.option = option;
        _email.sendMail()
            .then(result => {
                res.json({ message: "สำเร็จ", status: true });
                console.log(result);

            }).catch(err => {
                console.log(err);
                res.end(err.message);
            })
    });

module.exports = router;