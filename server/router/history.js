const express = require('express');
const router = express.Router();
const Email = require('../email/send');
const mongodb = require('../database/mongodb');
const db = new mongodb();
const option = {
    url: "mongodb://localhost:27017/",
    databaseName: "M-movies"
}

db.set = option;
router.route("/")
    .get((req, res) => {
        db.find("history")
            .then(r => {
                res.json(r);
            })
            .catch(err => {
                console.log(err)
                res.end(err.message);
            })
    })
    .post((req, res) => {
        const message = req.body;
        const { email, amount, name, price, total } = req.body;
        const _email = new Email();
        const option = {
            from: 'pichitkh58@gmail.com',
            to: email,
            subject: 'M-Movies',
            html: `<p>สวัสดีค่ะ คุณ ${email}</p><p>เมื่อสักครู่ คุณได้ทำการซื้อตั๋วหนังเรื่อง ${name} จำนวน ${amount} ที่นั่ง ที่นั่งละ ${price} บาท รวมเป็นเงิน ${total} บาท</p><p>ขอบคุณที่ใช้บริการค่ะ</p>`
        }
        _email.option = option;

        db.insertOne("history", message).then(r => {
            _email.sendMail()
                .then(result => {
                    res.json({ message: "สำเร็จ", status: true });
                    console.log(result);
                }).catch(err => {
                    console.log(err);
                    res.end(err.message);
                })
        })
    })
    .delete((req, res) => {
        const _id = db.decryObjectId(req.query._id);

        db.deleteOne("history", { _id })
            .then(r => {
                console.log(r.result);
                res.json(r.result);
            })
            .catch(err => {
                console.log(err);
                res.end(err.message);
            })
    })

module.exports = router;