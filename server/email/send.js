class Email {

    set auth(auth) {
        this._auth = auth;
    }

    get auth() {
        if (this._auth) {
            return this._auth;
        } else {
            return require('./config').auth;
        }
    }

    set option(option) {
        this._option = option;
    }

    get option() {
        if (this._option) {
            return this._option;
        } else {
            return require('./config').option;
        }
    }

    get createTransport() {
        const { user, pass } = this.auth;
        const nodemailer = require('nodemailer');
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: { user, pass }
        });

        return transporter;
    }

    sendMail() {
        return new Promise((resolove, reject) => {
            this.createTransport.sendMail(this.option, function (error, info) {
                if (error) {
                    reject(error);
                } else {
                    resolove('Email sent: ' + info.response);
                }
            });
        })

    }
}

module.exports = Email;