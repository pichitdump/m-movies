class Mongodb {

    set set(url) {
        this.config = url;
    }

    get set() {
        return this.config;
    }

    get mongoConnect() {
        const { url } = this.config;
        const MongoClient = require('mongodb').MongoClient;
        return new Promise((resolove, reject) => {
            MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
                if (err) return reject(err);
                resolove(db);
            });
        })
    }

    insertOne(collection, data) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).insertOne(data, function (err, res) {
                    if (err) return reject(err);
                    console.log("1 doccument inserted");
                    resolove(res);
                    db.close();
                });
            })
        })

    }

    insertMany(collection, data) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).insertMany(data, function (err, res) {
                    if (err) return reject(err);
                    console.log("Number of documents inserted: " + res.insertedCount);
                    resolove(res);
                    db.close();
                });
            })
        })
    }

    findOne(collection, condition = {}) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).findOne(condition, function (err, result) {
                    if (err) return reject(err);
                    resolove(result);
                });
            })
        })
    }

    find(collection, options = { condition: {}, projection: {} }) {
        const { databaseName } = this.config;
        const { condition, projection } = options;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).find(condition, { projection }).toArray(function (err, result) {
                    if (err) return reject(err);
                    resolove(result);
                });
            })
        })
    }

    sort(collection, condition = {}) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).find().sort(condition).toArray(function (err, result) {
                    if (err) return reject(err);
                    resolove(result);
                });
            })
        })
    }

    deleteOne(collection, condition = {}) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).deleteOne(condition, function (err, obj) {
                    if (err) return reject(err);
                    console.log("1 document deleted");
                    resolove(obj);
                    db.close();
                });
            })
        })
    }

    deleteMany(collection, condition = {}) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).deleteMany(condition, function (err, obj) {
                    if (err) return reject(err);
                    console.log("1 document deleted");
                    resolove(obj);
                    db.close();
                });
            })
        })
    }

    updateOne(collection, options = { condition: {}, values: {} }) {
        const { databaseName } = this.config;
        const { condition, values } = options;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).updateOne(condition, { $set: values }, function (err, res) {
                    if (err) return reject(err);
                    console.log("1 document updated");
                    resolove(res);
                    db.close();
                });
            })
        })
    }

    updateMany(collection, options = { condition: {}, values: {} }) {
        const { databaseName } = this.config;
        const { condition, values } = options;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).updateMany(condition, { $set: values }, function (err, res) {
                    if (err) return reject(err);
                    console.log(res.result.nModified + " document(s) updated");
                    resolove(res);
                    db.close();
                });
            })
        })
    }

    dropCollection(collection, condition = {}) {
        const { databaseName } = this.config;
        return new Promise((resolove, reject) => {
            this.mongoConnect.then(db => {
                const dbo = db.db(databaseName);
                dbo.collection(collection).drop(function (err, delOK) {
                    if (err) return reject(err);
                    if (delOK) console.log("Collection deleted");
                    resolove();
                    db.close();
                });
            })
        })
    }

    decryObjectId(objectId) {
        const ObjectId = require('mongodb').ObjectID;
        return ObjectId(objectId);
    }

    help(func) {
        return func.toString();
    }

}

module.exports = Mongodb;
