import React, { Component } from 'react';
import NavBar from '../../components/header/navbar.component';
import Carousel from '../../components/carousel/carousel.component';
import ProfileFooter from '../../components/footer/profile.footer.component';
import { connect } from 'react-redux';
import { GetMovies } from '../../actions/movies.action';
import { SendMail, SearchMovies } from '../../actions/members.action';
import ProductList from '../../components/items/itemList.component';
import BtnGroup from '../../components/button/btn-group.component';
import { withRouter } from 'react-router-dom';
import SweetAlert from '../../components/alert/alert.component';
import ReactLoading from 'react-loading';

class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = { movies: [], alert: false };
        this.addItem = this.addItem.bind(this);
        this.sortName = this.sortName.bind(this);
        this.sortPrice = this.sortPrice.bind(this);
        this.scrollTo = this.scrollTo.bind(this);
        this.alertState = this.alertState.bind(this);
        this.search = this.search.bind(this);

        this.home_ref = React.createRef();
        this.list_ref = React.createRef();
        this.contact_ref = React.createRef();
    }

    componentWillMount() {
        this.props.GetMovies();
        window.scrollTo(0, 0);
    }

    componentDidUpdate() {
        if (this.props.movies && this.props.movies.length !== this.state.movies.length) {
            this.sortPrice();
        }
    }

    addItem(item) {
        this.props.history.push({
            pathname: 'reservations',
            state: { item }
        });
    }

    sortName() {
        const sort = this.props.movies.sort((a, b) => {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
        this.setState({ movies: sort });
    }

    sortPrice() {
        const sort = this.props.movies.sort((a, b) => a.price - b.price);
        this.setState({ movies: sort });
    }

    scrollTo(value) {
        switch (value) {
            case 1: setTimeout(() => {
                this.home_ref.current.scrollIntoView();
            }, 50);
                return;
            case 2: setTimeout(() => {
                this.list_ref.current.scrollIntoView();
            }, 50);
                return;
            case 3: setTimeout(() => {
                this.contact_ref.current.scrollIntoView();
            }, 50);
                return;
            default: return;
        }
    }

    alertState(state) {
        this.setState({ alert: state })
    }

    search(value) {
        this._isUpdate = true;
        this.props.SearchMovies(value);
    }

    render() {
        const { movies } = this.state;
        const { formValues, SendMail } = this.props;
        const option = {
            type: 'success',
            title: 'ส่งเรียบร้อย',
            text: 'อีกสักครู่ ทางทีมงานจะติดต่อกลับ',
            confirmButtonColor: "#00d084"
        }
        return (
            <div className="h-100">
                <div ref={this.home_ref} className="h-100 mb-5 ">
                    <NavBar scrollTo={this.scrollTo} _ref={{ home: this.home_ref, list: this.list_ref, contact: this.contact_ref }} onSubmitSearch={this.search} />
                    <Carousel />
                </div>
                <div ref={this.list_ref}>
                    <BtnGroup sortName={this.sortName} sortPrice={this.sortPrice} />
                    {movies && movies.length > 0 && (
                        <div className="container my-5">
                            <ProductList products={movies} onAddItem={this.addItem} />
                        </div>
                    )}
                    {movies && movies.length === 0 && <ReactLoading type="bars" color="#fff" height="10%" width="10%" className="mx-auto mb-5"/>}
                </div>
                <div ref={this.contact_ref}>
                    <ProfileFooter onMemberSubmit={() => {
                        SendMail(formValues)
                        this.alertState(true);
                    }} />
                </div>
                {this.state.alert && (<SweetAlert option={option} setState={this.alertState} type="contact" />)}
            </div>
        )
    }
}

function mapStateToProps({ movies, form }) {
    if (form && form.ProfileFooter && movies.length > 0) {
        return { formValues: form.ProfileFooter.values, movies }
    }
    if (form && form.ProfileFooter) {
        return { formValues: form.ProfileFooter.values }
    }
    if (movies.length > 0) {
        return { movies }
    }
    return {};
}
export default withRouter(connect(mapStateToProps, { GetMovies, SendMail, SearchMovies })(HomePage));