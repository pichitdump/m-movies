import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import SweetAlert from '../../components/alert/alert.component';
import Jumbotron from '../../components/header/Jumbotron.component';
import ReserveContent from '../../components/reserve/reserve.component';
import { connect } from 'react-redux';
import { AddHistory } from '../../actions/members.action';

class ReservePage extends Component {

    constructor(props) {
        super(props);
        this.state = { alert: false, movie: {}, order: {}, option: {} };
        this.alertState = this.alertState.bind(this);
        this.addOrder = this.addOrder.bind(this);
        this.toHomePage = this.toHomePage.bind(this);
        this.onAddHistory = this.onAddHistory.bind(this);
    }

    componentWillMount() {
        if (this.props.location.state) {
            this.setState({ movie: this.props.location.state.item });
            return window.scrollTo(0, 0);
        } else {
            return this.props.history.push('/');
        }
    }

    alertState(state) {
        this.setState({ alert: state })
    }

    addOrder() {
        const { movie: { name, price } } = this.state;
        let { formValues: { amount, email } } = this.props;
        amount = parseInt(amount);
        const total = price * amount;
        const newFormValues = { email, amount, name, price, total };
        const option = {
            type: 'info',
            title: 'ชำระเงิน',
            text: `ยอดรวมทั้งหมด ${total} บาท`,
            input: 'number',
            inputPlaceholder: 'ป้อนยอดชำระเงิน',
            confirmButtonColor: "#00d084"
        }

        this.setState({ alert: true, order: newFormValues, option: option });
    }

    onAddHistory(result) {
        this.props.AddHistory(result);
    }

    toHomePage() {
        this.props.history.push('/');
    }

    render() {
        const { movie: { name, detail, poster, price, youtube }, order, option } = this.state;
        return (
            <div className="h-100">
                <Jumbotron name={name} detail={detail} onBack={this.toHomePage} />
                <ReserveContent name={name} price={price} poster={poster} youtube={youtube} onAddOrder={this.addOrder} />
                {this.state.alert && (<SweetAlert option={option} setState={this.alertState} formValues={order} onAddHistory={this.onAddHistory} onBack={this.toHomePage} type="input"/>)}
            </div>
        )

    }
}

function mapStateToProps({ form }) {
    if (form && form.ReserveContent) {
        return { formValues: form.ReserveContent.values };
    }
    return { formValues: null }

}

export default withRouter(connect(mapStateToProps, { AddHistory })(ReservePage));