import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { GetHistory, RemoveHistory } from '../../actions/history.action';
import { GetMovies, RemoveMovies, AddMovies } from '../../actions/movies.action';
import { connect } from 'react-redux';
import Table from '../../components/table/table.component';
import Product from '../../components/Product/Product.component';

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = { history: [], movies: [], page: 'history' };
        this.removeTableHistory = this.removeTableHistory.bind(this);
        this.removeTableMovies = this.removeTableMovies.bind(this);
    }

    componentWillMount() {
        this.props.GetHistory();
        this.props.GetMovies();
    }

    componentDidUpdate() {
        if (this.props.history && this.props.history.length !== this.state.history.length) {
            this.setState({ history: this.props.history });
        }
        if (this.props.movies && this.props.movies.length !== this.state.movies.length) {
            this.setState({ movies: this.props.movies });
        }
    }

    removeTableHistory(id) {
        RemoveHistory(id).then(result => {
            this.props.GetHistory();
        });
    }

    removeTableMovies(id) {
        RemoveMovies(id).then(result => {
            this.props.GetMovies();
        });
    }

    to(value) {
        this.setState({ page: value });
        value === "movies" && this.props.GetMovies();
        value === "history" && this.props.GetMovies();
    }

    render() {
        const headerTableHistory = [{ label: "#" }, { label: "Email" }, { label: "Title" }, { label: "Price" }, { label: "Amount" }, { label: "Total" }, { label: "Payment" }, { label: "Change" }, { label: "Other" }, { label: " " }];
        const headerTableMovies = [{ label: "#" }, { label: "Title" }, { label: "price" }, { label: "URL Poster" }, { label: "URL Youtube" }, { label: " " }]

        const { formValues, AddMovies } = this.props;

        return (
            <div className="bg-light h-100">
                <header className="mb-5" style={{ padding: 50, backgroundColor: 'rgba(82,82,82,0.9)' }}>
                    <ul className="nav justify-content-center">
                        <li className="nav-item">
                            <Link className="nav-link" to="/admin" onClick={() => this.to("history")}>History</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/admin" onClick={() => this.to("movies")}>Movies</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/admin" onClick={() => this.to("addMovies")}>Add Movies</Link>
                        </li>
                    </ul>
                </header>
                {this.state.page === 'history' && <Table history={this.state.history} header={headerTableHistory} removeTable={this.removeTableHistory} />}
                {this.state.page === 'movies' && <Table history={this.state.movies} header={headerTableMovies} removeTable={this.removeTableMovies} />}
                {this.state.page === 'addMovies' && <Product onAddProduct={() => AddMovies(formValues)} />}
            </div>
        )
    }
}

function mapStateToProps({ history, movies, form }) {
    if (form && form.Product) {
        return { formValues: form.Product.values, history, movies }
    }
    return { history, movies };
}
export default connect(mapStateToProps, { GetHistory, GetMovies, AddMovies })(AdminPage);
