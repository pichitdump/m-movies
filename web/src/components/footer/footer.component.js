import React, { Component } from 'react';
import './footer.css';


class Footer extends Component {
    render() {
        return (
            <div id="footer">
                <div className="sticky-bottom p-5">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                            <ul className="list-unstyled list-inline social text-center">
                                <li className="list-inline-item"><a><i className="fa fa-facebook"></i></a></li>
                                <li className="list-inline-item"><a><i className="fa fa-twitter"></i></a></li>
                                <li className="list-inline-item"><a><i className="fa fa-instagram"></i></a></li>
                                <li className="list-inline-item"><a><i className="fa fa-google-plus"></i></a></li>
                                <li className="list-inline-item"><a target="_blank"><i className="fa fa-envelope"></i></a></li>
                            </ul>
                        </div>

                        <hr />
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                            <p><u><a href="http://www.traceon.co.th/index.php/home">TraceoN</a></u> is a member company of the Charoen Aksorn Holding Group.</p>
                            <p className="h6">&copy All right Reversed.</p>
                        </div>
                        <hr />
                    </div>
                </div >
            </div >
        )
    }
}

export default Footer;

{/* <div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> */ }
