import React, { Component } from 'react';
import Map from '../GoogleMap/googlemap.component';
import { reduxForm, Field } from 'redux-form';
import './footer.css';
class ProfileFooter extends Component {


    renderForm() {
        const fieldForm = [
            { label: "Name", name: "name", type: "text", required: true },
            { label: "Email", name: "email", type: "email", required: true },
            { label: "Phone", name: "phone", type: "number", required: true },
            { label: "Message", name: "message", type: "text", required: true, row: '4' }
        ]

        return fieldForm.map(({ label, name, type, required }, index) => {
            return <Field key={index} label={label} name={name} type={type} required={required} component={this.formFieldArray} />
        });
    }

    formFieldArray({ input, label, name, type, required, row }) {
        if (row) {
            return (
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">{label}</label>
                    <div className="col-sm-10">
                        <textarea className="form-control" name={name} placeholder={label} row={row}></textarea>
                    </div>
                </div>
            )
        }
        return (
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">{label}</label>
                <div className="col-sm-10">
                    <input type={type} className="form-control" name={name} placeholder={label} required={required} {...input} />
                </div>
            </div>
        )
    }


    render() {
        const { onMemberSubmit } = this.props;
        return (
            <div className="bg-light p-5">
                <div className="container h-100">
                    <div className="row mt-5">
                        <div className="col-md-12 col-lg-6">
                            <h1 className="text-center text-secondary">Contact Us</h1>
                            <h5 className="text-secondary"><span className="fa fa-user"></span> Pichit khonkam</h5>
                            <h5 className="text-secondary"><span className="fa fa-envelope"></span> pichitkh58@gmail.com</h5>
                            <h5 className="text-secondary"><span className="fa fa-phone-square"></span> 096 668 0842</h5>
                            <h1 className="text-center text-secondary my-4">OR</h1>
                            <form onSubmit={this.props.handleSubmit(onMemberSubmit)}>
                                {this.renderForm()}
                                <button className="btn btn-block btn-info mb-5">Send</button>
                            </form>
                        </div>
                        <div className="col-md-12 col-lg-6 map-size">
                            <Map />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ProfileFooter = reduxForm({ form: 'ProfileFooter' })(ProfileFooter);
export default ProfileFooter;