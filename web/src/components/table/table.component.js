import React, { Component } from 'react';

class Table extends Component {


    renderTableHeader(header) {
        // const header = [{ label: "#" }, { label: "Email" }, { label: "Title" }, { label: "Price" }, { label: "Amount" }, { label: "Total" }, { label: "Payment" }, { label: "Change" }, { label: "Other" }, { label: " " }]
        return header.map(({ label }, index) => {
            return (
                <th key={index} scope="col">{label}</th>
            )
        });
    }

    renderTableBody(history) {
        return history.map(({ _id, email, name, price, amount, total, payment, change, classify, poster, youtube }, index) => {
            return (
                <tr key={index}>
                    <th scope="row">{(index + 1)}</th>
                    {email && <td>{email}</td>}
                    {name && <td>{name}</td>}
                    {price && <td>{price}</td>}
                    {amount && <td>{amount}</td>}
                    {total && <td>{total}</td>}
                    {payment && <td>{payment}</td>}
                    {change && <td>{change}</td>}
                    {classify && <td>{this.formating(classify)}</td>}
                    {poster && <td>{poster}</td>}
                    {youtube && <td>{youtube}</td>}
                    <td><button className="btn btn-sm btn-danger" onClick={() => this.props.removeTable(_id)}><i className="fa fa-times"></i></button></td>
                </tr>
            )
        })
    }

    formating(classify) {
        let str = '';
        for (const i in classify) {
            str = str + i.replace("baht", "*") + classify[i] + ' ';
        }
        return str;
    }

    render() {
        return (
            <div className="bg-light">
                <article className="container">
                    <table className="table">
                        <thead className="thead-light">
                            <tr>
                                {this.renderTableHeader(this.props.header)}
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTableBody(this.props.history)}
                        </tbody>
                    </table>
                </article>
            </div>

        )
    }
}

export default Table;