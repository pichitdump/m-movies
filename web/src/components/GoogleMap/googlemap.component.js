import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
class Map extends Component {
    render() {
        const GoogleMapExample = withGoogleMap(props => (
            <GoogleMap
                defaultCenter={{ lat: 13.6843, lng: 100.5287 }}
                defaultZoom={11}
            >
                <Marker position={{ lat: 13.6843, lng: 100.5287 }} />
            </GoogleMap>
        ));
        return (
            <div className="h-100">
                <GoogleMapExample
                    containerElement={<div style={{ height: '100%', width: '100%' }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                />
            </div>
        );
    }
};
export default Map;