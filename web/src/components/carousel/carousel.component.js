import React, { Component } from 'react';
import './carousel.css';
class Carousel extends Component {
    render() {
        return (
            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        {/* <img className="bg-img d-block w-100" src="http://www.impawards.com/2017/posters/split_xxlg.jpg" alt="First slide" /> */}
                        <img className="bg-img d-block w-100" src="https://images2.alphacoders.com/887/887367.jpg" alt="Second slide" />
                    </div>
                    <div className="carousel-item">
                        <img className="bg-img d-block w-100" src="http://hdqwalls.com/wallpapers/skyscraper-movie-10k-ve.jpg" alt="First slide" />
                    </div>
                    <div className="carousel-item">
                        <img className="bg-img d-block w-100" src="https://images4.alphacoders.com/858/858245.png" alt="Third slide" />
                    </div>

                </div>

                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div >
        )
    }
}

export default Carousel;