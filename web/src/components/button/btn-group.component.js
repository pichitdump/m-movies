import React, { Component } from 'react';

class BtnGroup extends Component {


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-8 offset-2">
                        <div className="d-flex justify-content-center my-5">
                            <div className="btn-group">
                                <button type="button" className="btn text-dark bg-light" onClick={() => this.props.sortName()}>เรียงตามตัวอักษร</button>
                                <button type="button" className="btn text-dark bg-light" onClick={() => this.props.sortPrice()}>เรียงตามราคา</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default BtnGroup;