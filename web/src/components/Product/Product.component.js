import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class Product extends Component {


    renderForm() {
        const fieldForm = [
            { label: "Name", name: "name", type: "text", required: true },
            { label: "Price", name: "price", type: "number", required: true },
            { label: "Detail", name: "detail", type: "text", required: false },
            { label: "URL Poster", name: "poster", type: "text", required: true },
            { label: "URL Youtube", name: "youtube", type: "text", required: true }
        ]

        return fieldForm.map(({ label, name, type, required }, index) => {
            return <Field key={index} label={label} name={name} type={type} required={required} component={this.formFieldArray} />
        });
    }

    formFieldArray({ input, label, name, type, required }) {
        return (
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">{label}</label>
                <div className="col-sm-10">
                    <input type={type} className="form-control" name={name} placeholder={label} required={required} {...input} />
                </div>
            </div>
        )
    }


    render() {
        const { onAddProduct } = this.props;
        return (
            <div className="container">
                <form onSubmit={this.props.handleSubmit(onAddProduct)}>
                    {this.renderForm()}
                    <button className="btn btn-block btn-info mb-5">ADD</button>
                </form>
            </div>
        )
    }
}

Product = reduxForm({ form: "Product" })(Product)
export default Product;