import React, { Component } from 'react';
import './item.css';
class ProductItem extends Component {

    render() {
        const { name, price, poster } = this.props.product;
        return (

            <div id="item" className="col-12 col-sm-6 col-md-4 col-lg-3">
                <div className="card">
                    <img className="card-img-top" src={poster} alt="Card image cap" />
                    <div className="card-body d-flex flex-column">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text text-success text-right my-auto">{price} THB</p>
                        <button type="button" onClick={() => this.props.onAddItem(this.props.product)} className="mt-auto btn btn-block btn-success">จองเดี๋ยวนี้</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default ProductItem;