import React, { Component } from 'react';
import ProductItem from './item.component';

class ProductList extends Component {

    showProduct() {
        return this.props.products && this.props.products.map((product, index) => (
            <ProductItem key={index} product={product} onAddItem={this.props.onAddItem} />
        ));
    }

    render() {
        return (
            <div className="row">
                {this.showProduct()}
            </div>
        )
    }
}

export default ProductList;