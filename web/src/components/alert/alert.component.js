import React, { Component } from 'react';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { filter } from './filter.function';

class SweetAlert extends Component {

    constructor(props) {
        super(props);
        this.state = { option: {}, formValues: {} };
    }

    componentDidMount() {
        this.setState({ option: this.props.option, formValues: this.props.formValues })
    }

    renderAlertform(option, formValues) {
        const MySwal = withReactContent(Swal)
        setTimeout(() => {
            MySwal.fire(option)
                .then(({ value }) => {
                    const payment = value;
                    const change = payment - formValues.total;
                    if (change >= 0) {
                        const classify = filter(change);
                        const date = Date.now();
                        return { ...formValues, payment, change, date, classify };
                    } else {
                        const { option, formValues } = this.state;
                        this.renderAlertform(option, formValues)
                    }

                })
                .then(result => {

                    if (result) {
                        const option = {
                            type: 'success',
                            title: 'ชำระเงินเรียบร้อย',
                            text: `เงินทอน ${result.change} บาท จำแนกเป็น ${this.formating(result.classify)}`,
                            confirmButtonColor: "#00d084"
                        }
                        MySwal.fire(option)
                            .then(() => {
                                this.props.onAddHistory(result);
                                this.props.setState(false);
                                this.props.onBack();
                            });
                    }
                })
        }, 10);
    }

    formating(classify) {
        let str = '';
        for (const i in classify) {
            str = str + i.replace("baht", "*") + classify[i] + ' ';
        }
        return str;
    }

    renderAlertNotify(option) {
        const MySwal = withReactContent(Swal);
        MySwal.fire(option).then(() => this.props.setState(false));
    }

    render() {
        const { option, formValues } = this.state;

        // const option = {
        //     type: 'error',
        //     title: 'Warning',
        //     text: message,
        //     confirmButtonColor: "#00d084"
        // }
        return (
            <div>
                {this.props.type === "input" && this.renderAlertform(option, formValues)}
                {this.props.type === "contact" && this.renderAlertNotify(this.props.option)}
            </div>
        )
    }
}

export default SweetAlert;