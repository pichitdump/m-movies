export const filter = (money) => {
    var result = { '1000baht': 0, '500baht': 0, '100baht': 0, '50baht': 0, '20baht': 0, '10baht': 0, '5baht': 0, '2baht': 0, '1baht': 0 };
    while (money) {
        if (money - 1000 >= 0) {
            money -= 1000;
            result['1000baht'] += 1;
        } else if (money - 500 >= 0) {
            money -= 500;
            result['500baht'] += 1
        } else if (money - 100 >= 0) {
            money -= 100;
            result['100baht'] += 1
        } else if (money - 50 >= 0) {
            money -= 50;
            result['50baht'] += 1
        } else if (money - 20 >= 0) {
            money -= 20;
            result['20baht'] += 1
        } else if (money - 10 >= 0) {
            money -= 10;
            result['10baht'] += 1
        } else if (money - 5 >= 0) {
            money -= 5;
            result['5baht'] += 1
        } else if (money - 2 >= 0) {
            money -= 2;
            result['2baht'] += 1
        } else if (money - 1 >= 0) {
            money -= 1;
            result['1baht'] += 1
        }
    }
    for (const i in result) {
        if (result[i] === 0) {
            delete result[i]
        }
    }
    return result;
}