import React, { Component } from 'react';
import Youtube from '../../components/Youtube/youtube.component';
import { reduxForm, Field } from 'redux-form';

class ReserveContent extends Component {
    constructor(props) {
        super(props);

        this.youtubeRef = React.createRef();
    }

    renderForm() {
        const field = [
            { label: "อีเมล", name: "email", type: "email", required: true },
            { label: "จำนวน", name: "amount", type: "number", required: true }
        ]

        return field.map(({ label, name, type, required }, index) => {
            return (
                <Field key={index} label={label} name={name} type={type} required={required} component={this.formFieldArray} />
            )
        });
    }

    formFieldArray({ input, label, name, type, required }) {
        return (
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">{label}</span>
                </div>
                <input type={type} name={name} {...input} required={required} className="form-control" />
            </div>
        )
    }

    render() {
        const { name, poster, price, youtube, onAddOrder } = this.props;
        return (
            <div className="container py-5">
                <div className="row">
                    <div ref={this.youtubeRef} className="col-md-8 offset-md-2 mb-5">
                        {/* <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-5"> */}
                        <Youtube url={youtube} />
                    </div>
                    <div className="col-md-8 offset-md-2 mb-5">
                        {/* <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-5"> */}
                        <div id="card" className="container">
                            <div className="row">
                                <div className="col-6">
                                    <img src={poster} className="rounded" style={{ width: '100%', height: '100%' }} alt="Card image cap" />
                                </div>
                                <div className="col-6 py-2 pl-2">
                                    <h5 className="text-center">{name}</h5>
                                    <p className="text-right mt-4">ราคา {price} THB</p>
                                    <form className="form-group mt-4" onSubmit={this.props.handleSubmit(onAddOrder)}>
                                        {this.renderForm()}
                                        <button type="submit" className="btn btn-block btn-success">ยืนยัน</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ReserveContent = reduxForm({ form: 'ReserveContent' })(ReserveContent);
export default ReserveContent;