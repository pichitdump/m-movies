import React, { Component } from 'react';

class Youtube extends Component {


    render() {
        return (
            <div className="embed-responsive embed-responsive-4by3">
                <iframe className="embed-responsive-item" src={this.props.url}></iframe>
            </div>
        )
    }
}

export default Youtube;