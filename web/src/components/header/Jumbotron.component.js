import React, { Component } from 'react';
import './header.css';

class Jumbotron extends Component {

    render() {
        const { name, detail, onBack } = this.props;
        return (
            <div id="jumbotron" className="jumbotron jumbotron-fluid">
                <div className="container text-light">
                    <h1 className="display-4">{name}</h1>
                    <p className="lead">{detail}</p>
                    <button type="button" className="btn btn-light text-muted btn-lg" onClick={() => onBack()}>กลับ</button>
                </div>
            </div>
        )
    }
}

export default Jumbotron;

