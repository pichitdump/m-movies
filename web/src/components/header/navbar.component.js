import React, { Component } from 'react';
import './header.css';

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = { style: { padding: 20 }, active: 1, menutoggle: false, offsetHeight: undefined, formValues: '' };
        this.collapse = React.createRef();
        this.navbar = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        const { _ref } = this.props;
        let offsetHeight = { home: 0, list: 0, contact: 0 };
        window.addEventListener('scroll', (event) => {
            const scroll = event.target.scrollingElement.scrollTop;

            if (!this.state.offsetHeight) {
                _ref.home.current && (offsetHeight.home = _ref.home.current.offsetHeight);
                _ref.list.current && (offsetHeight.list = (_ref.list.current.offsetHeight - (_ref.list.current.offsetHeight * 0.2)) + offsetHeight.home);
                _ref.contact.current && (offsetHeight.contact = _ref.contact.current.offsetHeight + offsetHeight.list);
                this._isMounted && this.setState({ offsetHeight: { ...offsetHeight } })
            } else {
                if (scroll < this.state.offsetHeight.home) {
                    this.state.active = 1;
                } else if (scroll < this.state.offsetHeight.list) {
                    this.state.active = 2;
                } else if (scroll > this.state.offsetHeight.list) {
                    this.state.active = 3;
                }
                this._isMounted && this.setState({ active: this.state.active });
            }

            if (this._isMounted) {
                if (scroll > 0) {
                    this.state.style = { padding: '5px 20px' };
                    this.navbar.current.classList.remove('scrollonTop');
                    this.navbar.current.classList.add('scrollonBottom');
                } else {
                    this.state.style = { padding: '20px' }
                    this.navbar.current.classList.remove('scrollonBottom');
                    this.navbar.current.classList.add('scrollonTop');
                }
                this.setState({ style: this.state.style });
            }
        });




    }

    componentDidUpdate() {
        if (this.navbar.current.offsetWidth >= 992 && this.navbar.current.classList.contains('bg-light')) {
            this.navbar.current.classList.remove('navbar-light', 'bg-light');
            this.navbar.current.classList.add('navbar-dark');
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleClick(value) {
        this.props.scrollTo(value);

        if (this.navbar.current.offsetWidth < 992) {
            this.state.menutoggle = !this.state.menutoggle;
            this.collapse.current.classList.remove("show");
            this.navbar.current.classList.remove('navbar-light', 'bg-light');
            this.navbar.current.classList.add('navbar-dark');
        } else {
            this.navbar.current.classList.remove('navbar-light', 'bg-light');
            this.navbar.current.classList.add('navbar-dark');
        }

        this._isMounted && this.setState({ menutoggle: this.state.menutoggle, style: { padding: '20px' } });
    }

    handleMenuToggle() {
        if (this.state.menutoggle) {
            this.navbar.current.classList.remove('navbar-light', 'bg-light');
            this.navbar.current.classList.add('navbar-dark');
            this._isMounted && this.setState({ style: { padding: '20px' } })
        } else {
            this.navbar.current.classList.remove('navbar-dark');
            this.navbar.current.classList.add('navbar-light', 'bg-light');
        }

        this._isMounted && this.setState({ menutoggle: !this.state.menutoggle })
    }

    handleSubmit(e) {
        this.props.onSubmitSearch(this.state.formValues);
        e.preventDefault();
    }

    handleChange(e) {
        this.setState({ formValues: e.target.value });
    }

    render() {
        const { active } = this.state;
        return (
            <nav id="navbar" ref={this.navbar} className="navbar navbar-expand-lg navbar-dark fixed-top scrollonTop" style={this.state.style}>
                <div className="container">
                    <a className="navbar-brand">
                        <img src="https://www.picz.in.th/images/2018/10/24/kIzVgk.png" width="50" height="40" className="d-inline-block align-top" style={{ borderRadius: 10 }} alt="" />
                    </a>
                    <button className="navbar-toggler" type="button" onClick={() => this.handleMenuToggle()} data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div ref={this.collapse} className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav ml-auto">
                            <a className={((active === 1) ? "nav-item nav-link text-center active" : "nav-item nav-link text-center")} onClick={() => this.handleClick(1)} href="#">หน้าแรก <span className="sr-only">(current)</span></a>
                            <a className={((active === 2) ? "nav-item nav-link text-center active" : "nav-item nav-link text-center")} onClick={() => this.handleClick(2)} href="#">รายการ</a>
                            <a className={((active === 3) ? "nav-item nav-link text-center active" : "nav-item nav-link text-center")} onClick={() => this.handleClick(3)} href="#">ติดต่อ</a>
                        </div>
                        <form className="form-inline my-2 my-lg-0 form-row" onSubmit={this.handleSubmit}>
                            <div className="col-9">
                                <input className="form-control mr-sm-2 w-100 " type="search" value={this.state.formValues} onChange={this.handleChange} placeholder="Search" aria-label="Search" />
                            </div>
                            <div className="col-3">
                                <button className="btn btn-dark my-2 my-sm-0 w-100 " type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        )
    }
}

export default NavBar;

