import { combineReducers } from 'redux';
import movies from './movies.reducer';
import historys from './historys.reducer';
import { reducer as reduxForm } from 'redux-form';;

const rootReducer = combineReducers({
    movies: movies,
    history: historys,
    form: reduxForm
});

export default rootReducer;