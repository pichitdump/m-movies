import { GET_MOVIES, SEARCH_MOVIES } from '../actions/actions.type';

export default function (state = [], action) {
    switch (action.type) {
        case GET_MOVIES:
            return action.payload;
        case SEARCH_MOVIES:
            return action.payload;
        default:
            return state;
    }
}