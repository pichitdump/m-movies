import axios from 'axios';
import { reset } from 'redux-form';
import { GET_MOVIES } from './actions.type';

export const GetMovies = () => {
    return dispatch => {
        axios.get('https://pichitdev.com/api/m-movies/movies/one')
            .then(res => {
                dispatch({ type: GET_MOVIES, payload: res.data })
            })
            .catch(err => {
                console.log(err.message);
            })
    }
}

export const RemoveMovies = (_id) => {
    return axios.delete('https://pichitdev.com/api/m-movies/movies/one?_id=' + _id)
        .then(res => {
            // console.log(res.data);
            return (res.data);
        })
        .catch(err => {
            console.log(err.message);
        })
}

export const AddMovies = (product) => {
    return dispatch => {
        axios.post('https://pichitdev.com/api/m-movies/movies/one', product)
            .then(res => {
                // console.log(res.data);
                dispatch(reset('Product'));
            })
            .catch(err => {
                console.log(err.message);
            })
    }
}


