import axios from 'axios';
import { GET_HISTORY } from './actions.type';

export const GetHistory = () => {
    return dispatch => {
        axios.get('https://pichitdev.com/api/m-movies/history')
            .then(res => {
                dispatch({ type: GET_HISTORY, payload: res.data })
            })
            .catch(err => {
                console.log(err.message);
            })
    }
}

export const RemoveHistory = (_id) => {
    return axios.delete('https://pichitdev.com/api/m-movies/history?_id=' + _id)
        .then(res => {
            // console.log(res.data);
            return (res.data);
        })
        .catch(err => {
            console.log(err.message);
        })
}



