import axios from 'axios';
import { reset } from 'redux-form';
import { SEARCH_MOVIES } from './actions.type';

export const AddHistory = (order) => {
    return dispatch => {
        const message = { date: Date.now(), ...order };
        axios.post('https://pichitdev.com/api/m-movies/history', message)
            .then(result => {
                console.log(result.data);
                dispatch(reset('ReserveContent'));
            })
    }
}

export const SendMail = (contactMessage) => {
    return dispatch => {
        axios.post('https://pichitdev.com/api/m-movies/movies/contact', contactMessage)
            .then(result => {
                dispatch(reset('ProfileFooter'));
            })
    }
}

export const SearchMovies = (search) => {
    return dispatch => {
        axios.get('https://pichitdev.com/api/m-movies/movies/many?search=' + search)
            .then(result => {
                // console.log(result.data);
                dispatch({ type: SEARCH_MOVIES, payload: result.data });
            })
    }
}