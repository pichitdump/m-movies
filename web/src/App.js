import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from './containers/homePage/home.container';
import ReservePage from './containers/ReservePage/reserve.container';
import AdminPage from './containers/admin/admin.container';

class App extends Component {
  componentDidMount() {
    document.body.style.backgroundColor = "#343a40";

  }

  renderRouter() {
    return (
      <div className="h-100 w-100">
        <Switch>
          <Route exact path="/reservations" component={ReservePage} />
          <Route exact path="/admin" component={AdminPage} />
          <Route component={HomePage} />
        </Switch>
      </div>
    )
  }

  render() {
    return (
      <BrowserRouter>{this.renderRouter()}</BrowserRouter>
    )
  }
}

export default App;
